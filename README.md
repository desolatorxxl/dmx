# DMX test

## Equipment

[ETEC E310](https://www.etec-professional.de/product_info.php?language=en&products_id=138096)

![e310](doc/etec_e310.jpg)

[DSD TECH USB to DMX RS485 Adapter with FTDI Chip](https://www.amazon.com/DSD-TECH-RS485-Adapter-Lichtsteuerungskabel-Schwarz/dp/B07WV6P5W6/ref=sr_1_5)

![dsd_tech](doc/dsd_tech_usb_dmx_rs485_ftdi.jpg)

Set udev rules for FTDI device in `/etc/udev/rules.d/11-ftdi.rules`:

```
ATTR{manufacturer}=="FTDI", MODE="0777"
```

```
python3 -m venv dev
source dev/bin/activate
python3 dmx.py
```
