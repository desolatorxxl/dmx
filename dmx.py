import time
import random
from pyftdi.ftdi import Ftdi

Ftdi.show_devices()

url = 'ftdi://ftdi:232:AB0KP0FM/1'
port = Ftdi.create_from_url(url)
port.reset()
port.set_baudrate(baudrate=250000)
port.set_line_property(bits=8, stopbit=2, parity='N', break_=False)

assert port.is_connected

data = bytearray(513 * [0])

i = 255
r = random.randrange(0, 255)
g = random.randrange(0, 255)
b = random.randrange(0, 255)

while True:
    # blue
    data[4] = b
    # green
    data[3] = g
    # red
    data[2] = r
    # brigthness
    data[1] = i

    i = i - 10

    if i <= 0:
        i = 255
        r = random.randrange(0, 255)
        g = random.randrange(0, 255)
        b = random.randrange(0, 255)
        
    port.set_break(False)
    port.write_data(data)
    port.set_break(True)
    time.sleep(8/1000.0)
